package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/go-chi/chi"
)

type OperationRequest struct {
	Type string `json:"type"`
	ValueA int `json:"value-a"`
	ValueB int `json:"value-b"`
}

type OperationResponse struct {
	Value int `json:"value"`
}

func main() {
	SERVICE_PORT := os.Getenv("SERVICE_PORT")

	r := chi.NewRouter()
	r.Post("/ops", opsHandler)

	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", SERVICE_PORT), r))
}

func opsHandler(w http.ResponseWriter, r *http.Request) {
	var opRequest OperationRequest
	var opResponse OperationResponse

	opResponse.Value = 0

	err := json.NewDecoder(r.Body).Decode(&opRequest)
	if err == nil {
		switch opRequest.Type {
		case "add":
			opResponse.Value = opRequest.ValueA + opRequest.ValueB
		case "sub":	
			opResponse.Value = opRequest.ValueA - opRequest.ValueB
		}	
	}

	json.NewEncoder(w).Encode(opResponse)
}

func errorf(w http.ResponseWriter, code int, format string, a ...interface{}) {
	var out struct {
		Code    int    `json:"code"`
		Message string `json:"message"`
	}

	out.Code = code
	out.Message = fmt.Sprintf(format, a...)

	b, err := json.Marshal(out)
	if err != nil {
		http.Error(w, `{"code": 500, "message": "Could not format JSON for original message."}`, 500)
		return
	}

	http.Error(w, string(b), code)
}